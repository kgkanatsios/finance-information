<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this['Symbol'],
                'attributes' => [
                    'name' => $this['Company Name'],
                    'symbol' => $this['Symbol'],
                ],
            ],
            'links' => [
                'self' => url('/api/companies/' . $this['Symbol']),
            ]
        ];
    }
}
