<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HistoricalData extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'attributes' => [
                    'date' => date('Y-m-d', $this['date']),
                    'open' => $this['open'] ?? 0,
                    'high' => $this['high'] ?? 0,
                    'low' => $this['low'] ?? 0,
                    'close' => $this['close'] ?? 0,
                    'volume' => $this['volume'] ?? 0,
                ],
            ]
        ];
    }
}
