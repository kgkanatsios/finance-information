<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\HistoricalDataCollection;
use App\Events\FormSubmitted;
use App\Repositories\HistoricalData\HistoricalDataRepositoryInterface;
use App\Repositories\Company\CompanyRepositoryInterface;

class HistoricalDataController extends Controller
{
    private $historicalDataRepository;
    private $companyRepository;

    public function __construct(HistoricalDataRepositoryInterface $historicalDataRepository, CompanyRepositoryInterface $companyRepository)
    {
        $this->historicalDataRepository = $historicalDataRepository;
        $this->companyRepository = $companyRepository;
    }

    public function show(Request $request)
    {
        $data = request()->validate([
            'company' => 'required',
            'email' => 'required|email',
            'startDate' => 'required|date_format:Y-m-d|before_or_equal:' . date('Y-m-d'),
            'endDate' => 'required|date_format:Y-m-d|after_or_equal:startDate|before_or_equal:' . date('Y-m-d'),
        ]);

        $company = $this->companyRepository->getCompany($data['company']);

        FormSubmitted::dispatch($data['email'], $company['Company Name'], 'From ' . $data['startDate'] . ' to ' . $data['endDate']);
        return response(new HistoricalDataCollection($this->historicalDataRepository->getHistoricalData($data['startDate'],  $data['endDate'], $data['company'])), 200)->header('Content-Type', 'application/json');
    }
}
