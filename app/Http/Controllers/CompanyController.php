<?php

namespace App\Http\Controllers;

use App\Http\Resources\CompanyCollection;
use App\Http\Resources\Company as ResourcesCompany;
use App\Repositories\Company\CompanyRepositoryInterface;

class CompanyController extends Controller
{
    private $companyRepository;

    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function index()
    {
        return response(new CompanyCollection($this->companyRepository->all()), 200)->header('Content-Type', 'application/json');
    }

    public function show(string $symbol)
    {
        return response(new ResourcesCompany($this->companyRepository->getCompany($symbol)), 200)->header('Content-Type', 'application/json');
    }
}
