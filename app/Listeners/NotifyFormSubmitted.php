<?php

namespace App\Listeners;

use App\Events\FormSubmitted;
use App\Mail\FormSubmittedMail;
use Illuminate\Support\Facades\Mail;

class NotifyFormSubmitted
{
    /**
     * Handle the event.
     *
     * @param  FormSubmitted  $event
     * @return void
     */
    public function handle(FormSubmitted $event)
    {
        Mail::to($event->getEmail())->send(new FormSubmittedMail($event->getEmail(), $event->getSubject(),  $event->getMessage()));
    }
}
