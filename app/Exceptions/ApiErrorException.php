<?php

namespace App\Exceptions;

use Exception;

class ApiErrorException extends Exception
{
    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json([
            'errors' => [
                'code' => $this->getCode(),
                'title' => 'Error',
                'detail' => 'Error has occurred',
                'meta' => $this->getMessage(),
            ]
        ], $this->getCode())->header('Content-Type', 'application/json');
    }
}
