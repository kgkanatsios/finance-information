<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Company\CompanyRepositoryInterface;
use App\Repositories\Company\CompanyRepository;
use App\Repositories\HistoricalData\HistoricalDataRepositoryInterface;
use App\Repositories\HistoricalData\HistoricalDataRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CompanyRepositoryInterface::class, CompanyRepository::class);
        $this->app->bind(HistoricalDataRepositoryInterface::class, HistoricalDataRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
