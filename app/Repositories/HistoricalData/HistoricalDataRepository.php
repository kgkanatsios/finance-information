<?php

namespace App\Repositories\HistoricalData;

use Illuminate\Support\Facades\Http;
use App\Exceptions\ApiErrorException;
use Exception;

class HistoricalDataRepository implements HistoricalDataRepositoryInterface
{
    public function getHistoricalData($startDate, $endDate, $company)
    {
        try {
            $response = Http::withHeaders([
                'x-rapidapi-key' => env('HISTORICAL_DATA_RAPID_API_KEY'),
            ])->get(env('HISTORICAL_DATA_URL'), [
                'period1' => strtotime($startDate),
                'period2' => strtotime($endDate),
                'symbol' => $company,
            ]);

            return $response->json()["prices"];
        } catch (Exception $e) {
            throw new ApiErrorException($e);
        }
    }
}
