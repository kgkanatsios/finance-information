<?php

namespace App\Repositories\HistoricalData;

interface HistoricalDataRepositoryInterface
{
    public function getHistoricalData($startDate, $endDate, $company);
}
