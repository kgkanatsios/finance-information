<?php

namespace App\Repositories\Company;

use Illuminate\Support\Facades\Http;
use App\Exceptions\ApiErrorException;
use Exception;

class CompanyRepository implements CompanyRepositoryInterface
{
    public function all()
    {
        try {
            $response = Http::get(env('NASDAQ_LIST_URL'));
            if ($response->successful()) {
                return $response->json();
            }
            throw new ApiErrorException($response->serverError(), $response->status());
        } catch (Exception $e) {
            throw new ApiErrorException($e);
        }
    }
    public function getCompany($symbol)
    {
        $response = Http::get(env('NASDAQ_LIST_URL'));
        if ($response->successful()) {
            $collection = collect($response->json());
            $company = $collection->firstWhere('Symbol', $symbol);
            if (empty($company)) {
                throw new ApiErrorException('Company not found', 404);
            }
            return $company;
        }
        throw new ApiErrorException($response->serverError(), $response->status());
    }
}
