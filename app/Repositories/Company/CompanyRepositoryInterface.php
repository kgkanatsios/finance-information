<?php

namespace App\Repositories\Company;

interface CompanyRepositoryInterface
{
    public function all();
    public function getCompany($symbol);
}
