import Vue from "vue";
import Vuex from "vuex";

import Companies from "./modules/companies.js";
import Form from "./modules/Form.js";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        Companies,
        Form
    }
});
