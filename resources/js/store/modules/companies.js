const state = {
    loadCompanyData: false,
    companies: [],
    selectedCompany: {}
};

const getters = {
    loadCompanyData: state => {
        return state.loadCompanyData;
    },
    companies: state => {
        return state.companies;
    },
    selectedCompany: state => {
        return state.selectedCompany;
    }
};

const actions = {
    fetchCompanies({ commit }) {
        axios
            .get("/api/companies")
            .then(res => {
                commit("setCompanies", res.data.data);
                commit("setLoadCompanyData", true);
            })
            .catch(err => {
                console.log("Unable to fetch companies. Error:" + err);
            });
    },
    fetchCompany({ commit }, data) {
        axios
            .get("/api/companies/" + data)
            .then(res => {
                commit("setSelectedCompany", res.data.data);
            })
            .catch(err => {
                console.log("Unable to fetch company. Error:" + err);
            });
    },
    setSelectedCompany({ commit }, data) {
        commit("setSelectedCompany", {
            id: data.value,
            attributes: { name: data.name, symbol: data.value }
        });
    }
};

const mutations = {
    setLoadCompanyData(state, loadCompanyData) {
        state.loadCompanyData = loadCompanyData;
    },
    setCompanies(state, companies) {
        state.companies = companies;
    },
    setSelectedCompany(state, selectedCompany) {
        state.selectedCompany = selectedCompany;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
