const state = {
    formSubmitted: false,
    formLoadData: false,
    formResponse: []
};

const getters = {
    formSubmitted: state => {
        return state.formSubmitted;
    },
    formLoadData: state => {
        return state.formLoadData;
    },
    formResponse: state => {
        return state.formResponse;
    }
};

const actions = {
    submitForm({ commit }, data) {
        commit("setFormLoadData", false);
        commit("setFormSubmitted", true);
        commit("setFormResponse", []);
        axios
            .post("/api/historical-data", data)
            .then(res => {
                commit("setFormLoadData", true);
                commit("setFormResponse", res.data.data);
            })
            .catch(err => {
                console.log("Unable to submit form. Error:" + err);
            });
    }
};

const mutations = {
    setFormSubmitted(state, formSubmitted) {
        state.formSubmitted = formSubmitted;
    },
    setFormLoadData(state, formLoadData) {
        state.formLoadData = formLoadData;
    },
    setFormResponse(state, formResponse) {
        state.formResponse = formResponse;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
