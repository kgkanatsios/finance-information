# Finance Historical Data

This web application was developed using [Laravel framework](https://laravel.com/)

## Server Requirements

In the following link you can read the server requirements.
[https://laravel.com/docs/7.x/installation#server-requirements](https://laravel.com/docs/7.x/installation#server-requirements)

## Installation

```sh
$ composer install
$ cp .env.example .env
$ php artisan key:generate
```

## Initial Configuration

1. Set the value of `MAIL_USERNAME` variable.
2. Set the value of `MAIL_PASSWORD` variable.
3. Set the value of `MAIL_ENCRYPTION` variable.
4. Set the value of `MAIL_FROM_ADDRESS` variable.
5. Set the value of `HISTORICAL_DATA_RAPID_API_KEY` variable.

## Testing

```sh
$ php artisan test
```

## Local Development Server

Artisan command will start a development server at http://localhost:8000

```sh
$ php artisan serve
```
