<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    /**
     * @return void
     */
    public function testGetCompanies()
    {
        Http::fake([
            'https://pkgstore.datahub.io/*' => Http::response([["Company Name" => "iShares MSCI All Country Asia Information Technology Index Fund", "Symbol" => "AAIT"], ["Company Name" => "American Airlines Group, Inc.", "Symbol" => "AAL"]], 200),
        ]);
        $this->get('/api/companies')->assertStatus(200)->assertExactJson([
            "data" => [
                [
                    "data" => [
                        "id" => "AAIT",
                        "attributes" => [
                            "name" => "iShares MSCI All Country Asia Information Technology Index Fund",
                            "symbol" => "AAIT",
                        ]
                    ],
                    "links" => [
                        "self" => url('/api/companies/AAIT')
                    ]
                ],
                [
                    "data" => [
                        "id" => "AAL",
                        "attributes" => [
                            "name" => "American Airlines Group, Inc.",
                            "symbol" => "AAL",
                        ]
                    ],
                    "links" => [
                        "self" => url('/api/companies/AAL')
                    ]
                ]
            ],
            "links" => [
                "self" => url('/api/companies')
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testGetCompany()
    {
        Http::fake([
            'https://pkgstore.datahub.io/*' => Http::response([["Company Name" => "iShares MSCI All Country Asia Information Technology Index Fund", "Symbol" => "AAIT"], ["Company Name" => "American Airlines Group, Inc.", "Symbol" => "AAL"]], 200),
        ]);
        $this->get('/api/companies/AAIT')->assertStatus(200)->assertExactJson([
            "data" => [
                "id" => "AAIT",
                "attributes" => [
                    "name" => "iShares MSCI All Country Asia Information Technology Index Fund",
                    "symbol" => "AAIT",
                ]
            ],
            "links" => [
                "self" => url('/api/companies/AAIT')
            ]
        ]);
    }
}
