<?php

namespace Tests\Feature;

use App\Events\FormSubmitted;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class HistoricalDataTest extends TestCase
{
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        Event::fake();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHistoricalDataRequired()
    {
        $response = $this->post('/api/historical-data', [
            'company' => '',
            'email' => '',
            'startDate' => '',
            'endDate' => '',
        ]);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "code" => 422,
                "meta" => [
                    "company" => ["The company field is required."],
                    "email" => ["The email field is required."],
                    "startDate" => ["The start date field is required."],
                    "endDate" => ["The end date field is required."],
                ]
            ]
        ]);
    }

    public function testHistoricalDataCompanyExists()
    {
        $response = $this->post('/api/historical-data', [
            'company' => 'InvalidCompany',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y-m-d', strtotime("-2 Months")),
            'endDate' => date('Y-m-d', strtotime("-1 Month")),
        ]);

        $response->assertStatus(404)->assertJson([
            "errors" => [
                "code" => 404,
                "meta" => "Company not found"
            ]
        ]);
    }

    public function testHistoricalDataEmailIsValid()
    {
        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => 'invalid.email.com',
            'startDate' => date('Y-m-d', strtotime("-2 Months")),
            'endDate' => date('Y-m-d', strtotime("-1 Month")),
        ]);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "code" => 422,
                "meta" => [
                    "email" => ["The email must be a valid email address."],
                ]
            ]
        ]);
    }

    public function testHistoricalDataDatesAreValid()
    {
        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y/m/d', strtotime("-2 Months")),
            'endDate' => date('Y/m/d', strtotime("-1 Month")),
        ]);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "code" => 422,
                "meta" => [
                    "startDate" => ["The start date does not match the format Y-m-d."],
                    "endDate" => ["The end date does not match the format Y-m-d."],
                ]
            ]
        ]);
    }

    public function testHistoricalDataStartDateIsLowerOrEqualThanToday()
    {
        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y-m-d', strtotime("+1 Month")),
            'endDate' => date('Y-m-d', strtotime("+2 Months")),
        ]);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "code" => 422,
                "meta" => [
                    "startDate" => ["The start date must be a date before or equal to " . date('Y-m-d') . "."],
                ]
            ]
        ]);
    }

    public function testHistoricalDataEndDateIsLowerOrEqualThanToday()
    {
        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y-m-d', strtotime("+1 Month")),
            'endDate' => date('Y-m-d', strtotime("+2 Months")),
        ]);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "code" => 422,
                "meta" => [
                    "endDate" => ["The end date must be a date before or equal to " . date('Y-m-d') . "."],
                ]
            ]
        ]);
    }

    public function testHistoricalDataEndDateIsGreaterOrEqualThanStartDate()
    {
        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y-m-d', strtotime("-1 Month")),
            'endDate' => date('Y-m-d', strtotime("-2 Months")),
        ]);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "code" => 422,
                "meta" => [
                    "endDate" => ["The end date must be a date after or equal to start date."],
                ]
            ]
        ]);
    }
    public function testHistoricalDataEndDateCouldBeEqualThanStartDate()
    {
        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y-m-d', strtotime("-1 Month")),
            'endDate' => date('Y-m-d', strtotime("-1 Month")),
        ]);

        $response->assertStatus(200);
    }
    public function testHistoricalDataResponse()
    {
        Http::fake([
            'https://apidojo-yahoo-finance-v1.p.rapidapi.com/*' => Http::response([
                "prices" => [
                    [
                        "date" => strtotime("-1 Month"),
                        "open" => 1102.239990234375,
                        "high" => 1111.77001953125,
                        "low" => 1098.1700439453125,
                        "close" => 1111.25,
                        "volume" => 991600,
                        "adjclose" => 1111.25
                    ],
                    [
                        "date" => strtotime("-2 Months"),
                        "open" => 1102.239990234375,
                        "high" => 1111.77001953125,
                        "low" => 1098.1700439453125,
                        "close" => 1111.25,
                        "volume" => 991600,
                        "adjclose" => 1111.25
                    ]
                ]
            ], 200),
        ]);

        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y-m-d', strtotime("-2 Months")),
            'endDate' => date('Y-m-d', strtotime("-1 Month")),
        ]);
        $response->assertStatus(200)->assertExactJson([
            "data" => [
                [
                    "data" => [
                        "attributes" => [
                            "date" => date('Y-m-d', strtotime("-2 Months")),
                            "open" => 1102.239990234375,
                            "high" => 1111.77001953125,
                            "low" => 1098.1700439453125,
                            "close" => 1111.25,
                            "volume" => 991600
                        ]
                    ]
                ],
                [
                    "data" => [
                        "attributes" => [
                            "date" => date('Y-m-d', strtotime("-1 Month")),
                            "open" => 1102.239990234375,
                            "high" => 1111.77001953125,
                            "low" => 1098.1700439453125,
                            "close" => 1111.25,
                            "volume" => 991600
                        ]
                    ]
                ]
            ]
        ]);
    }
    public function testHistoricalDataResponseDispatchEvent()
    {
        Http::fake([
            'https://apidojo-yahoo-finance-v1.p.rapidapi.com/*' => Http::response([
                "prices" => [
                    [
                        "date" => strtotime("-1 Month"),
                        "open" => 1102.239990234375,
                        "high" => 1111.77001953125,
                        "low" => 1098.1700439453125,
                        "close" => 1111.25,
                        "volume" => 991600,
                        "adjclose" => 1111.25
                    ],
                    [
                        "date" => strtotime("-2 Months"),
                        "open" => 1102.239990234375,
                        "high" => 1111.77001953125,
                        "low" => 1098.1700439453125,
                        "close" => 1111.25,
                        "volume" => 991600,
                        "adjclose" => 1111.25
                    ]
                ]
            ], 200),
        ]);

        $response = $this->post('/api/historical-data', [
            'company' => 'GOOG',
            'email' => $this->faker->safeEmail(),
            'startDate' => date('Y-m-d', strtotime("-2 Months")),
            'endDate' => date('Y-m-d', strtotime("-1 Month")),
        ]);

        $response->assertStatus(200);
        Event::assertDispatched(FormSubmitted::class);
    }
}
